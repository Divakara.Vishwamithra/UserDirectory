const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
    username: String,
    address: String,
    contact: Number,
    email:String
}, {
    timestamps: true
});

module.exports = mongoose.model('User', UserSchema);