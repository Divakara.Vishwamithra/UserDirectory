import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import 'materialize-css/dist/css/materialize.css';
import 'materialize-css/dist/css/materialize.min.css';
import Router from './components/router.js';


class App extends Component {
  render() {
    return (
      <div>
        <Router/>
      </div>
    );
  }
}

export default App;
