import React from 'react';
import {Route,Switch} from 'react-router-dom';
import UserDir from './userDir';
import User from './user';

const Router = () => (
	<main>
		<Switch>
       <Route exact path='/' component={UserDir} />
       <Route exact path='/user' component={UserDir} />
       <Route exact path='/user/new' component={User} />
       <Route exact path='/user/:id/edit' component={User} />
	  </Switch>
	</main>   
)

export default Router;