import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import $ from 'jquery';
import axios from 'axios'; 
import Materialize from 'materialize-css/dist/js/materialize.js';
import 'font-awesome/css/font-awesome.min.css';



class UserDir extends Component {
  constructor(props) {    
    super(props);
    this.state={
      users: []
    };
  }

  componentWillMount(){
    
    var url='http://localhost:3001/users';
    axios.request({
      method:'get',
      url:url
    }).then(response => {

     
        this.setState($.extend({},response.data));
      
    if(response.data.statusMessage === "error"){
        Materialize.toast(response.data.message, 3000, 'rounded green lighten-1')
      }          
    })
    .catch(error => {
      // swal(error.response.data.message.name.message);
    })
  }

  render() {
    var users = this.state.users.map((user, index) => (
      <tr key={index}>
        
        <td> {user.username} </td>
        <td> {user.address} </td>
        <td> {user.contact} </td>
        <td> {user.email} </td>
        <td>
          <div className="tooltip">
          <span className="tooltiptext"></span>
            <Link to={"/user/"+user._id+"/edit"} className="btn-small waves-effect blue darken-1 waves-light btn btn-small"><i className="fa fa-edit"></i></Link>
          </div>
          &emsp;
          
        </td>
        <td>
          <div className="tooltip">
          <span className="tooltiptext"></span>
          <Link to={"/user"} className="btn-small waves-effect blue darken-1 waves-light btn btn-small"><i className="fa fa-trash"></i></Link>
          
          </div>
          &emsp;
          
        </td>
      </tr>
      ))
    return (
      <div className="container z-depth-1 hoverable">
        <div className="row">
          <h5 className="left-align col m3">User Directory</h5><br />
          <div className="col m9 right-align">
            <Link to="/user/new" className="btn-small waves-effect blue darken-1 waves-light btn btn-small" name="new" id="new_userdetailes"><i className="fa fa-plus left"></i>&nbsp;Add New User</Link> 
          </div>
        </div>
        <div>
          <table id='table' className="table responsive-table striped">
            <thead className="table_header">
              <tr>
                <th className="col m2">USER NAME</th>
                <th className="col m2">ADDRESS</th>
                <th className="col m2">CONTACT</th>
                <th className="col m2">Email</th>
              </tr>
            </thead>
            <tbody>
              {users}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default UserDir;
