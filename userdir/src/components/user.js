import React, { Component } from 'react';
import 'font-awesome/css/font-awesome.min.css';
import $ from 'jquery';
import axios from 'axios'; 
import 'materialize-css/dist/css/materialize.min.css';
import Materialize from 'materialize-css/dist/js/materialize.js';
import 'react-select/dist/react-select.css';
import {Link} from 'react-router-dom';


class User extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users:[]
     };
  }

  componentWillMount(){
    var url=window.location.pathname;
    var edit=null;
    if (url !== '/user/new' ){
      edit = "true"
    }
    else {
      edit = "false"
    }
    axios.request({
      method:'get',
      url:'http://localhost:3001/users'
    }).then(response => {
      
        this.setState($.extend({},response.data,{edit: edit}));
        if (edit === "false"){this.setState()}  
      
         
    })
    .catch(error => {
      alert(error.response.data.message);
    })
  }

  push_data(pair,obj){
    pair['username'] = obj.refs.username.value;
    pair['address'] = obj.refs.address.value;
    pair['contact'] = obj.refs.contact.value;
    pair['email'] = obj.refs.email.value;
    return pair;
  }

  handlestateSubmit(_id,event){
    var self=this;
    var url = null;
    var type = null;
    var pair = {};
   
    pair=this.push_data(pair, this);
    
    if (_id){
      url = "/user/"+_id;
      type = 'PUT'
    }
    else {
      url = "/user";
      type = 'POST'
    }
    
    axios.request({
      method:type,
      url:'http://localhost:3001/users',
      data: pair
    }).then(response => {
      
        self.props.history.push({
          pathname: '/user',
          state: {user_message: response.data.message}
          })
      
       
    })
    .catch(error => {
      alert(error.response.data.message);
    })
    event.preventDefault();
  }

  setValue(key,event){
    alert(key)
  }

    render() {
    if (this.state.edit) {
      $(document).ready(function() {
        Materialize.updateTextFields();
      });
      var users = this.state.users
      var edit = this.state.edit === "true" ? true : false;
      return (
        <div className="container z-depth-1 hoverable">
          <div className="row">
            <div className="col s1"></div>
            <form id="user_validate" className="col s10" onSubmit={ this.handlestateSubmit.bind(this, this.state.users._id)}>
              <div>
                {edit ? <h5 className="center-align heading-blue">Edit User</h5> :  <h5 className="center-align heading-blue">New User</h5>}
              </div>
              <br/>
              <div className="row">
                <div className="input-field col s6">
                  <h6 className="left-align">User Name<span className="star"> *</span></h6>
                  <input
                    
                    name="username" 
                    id="username"
                    ref="username" 
                    placeholder="Enter User Name"
                    className="alignLeft not-allowed"
                    defaultValue={this.state.users.username}
                    required
                     />
                </div>
                <div className="input-field col s6">
                  <h6 className="left-align">Address<span className="star"> *</span></h6>
                  <input     
                    name="address" 
                    id="address"
                    ref="address" 
                    placeholder="Enter The Address"
                    className="alignLeft not-allowed"
                    defaultValue={this.state.users.address}
                    required
                    />
                </div>
                <div className="input-field col s6">
                  <h6 className="left-align">Contact<span className="star"> *</span></h6>
                  <input 
                    name="contact" 
                    id="contact"
                    ref="contact"
                    placeholder="Enter Contact Number"
                    className="alignLeft not-allowed"
                    defaultValue={this.state.users.contact}
                    required
                    />
                </div>
                <div className="input-field col s6">
                  <h6 className="left-align">Email<span className="star"> *</span></h6>
                  <input
                    name="email" 
                    id="email"
                    ref="email" 
                    placeholder="Enter Mail Id"
                    className="alignLeft not-allowed"                    
                    defaultValue={this.state.users.email}
                    required
                    />
                </div>
              </div>
              <div className="col s12 center">
              <Link to="/user" className="waves-effect waves-blue btn ButtonIndex" style={{marginBottom: '10px'}}>Back</Link>&emsp;
              <input type="submit" style={{marginBottom: '10px'}} className="waves-effect waves-green btn ButtonIndex" value={edit ? "Update" : "Submit" } /></div>
            </form>
          </div>
        </div>
      )
    }
    else {
      return (
        <div> <img src="http://datainflow.com/wp-content/uploads/2017/09/loader.gif" alt="loading" style={{height:"400px",width:"700px"}}/></div>
      )
    }
    
  }
}

export default User; 